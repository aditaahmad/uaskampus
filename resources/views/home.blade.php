@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"><h3>Tugas UAS</h3></div>

				<div class="panel-body">
					
					<form class="form-horizontal" method="" action="">
						<div class="form-group">
							<div class="col-md-12 text-center"><h4>Pemrograman Framework</h4></div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Nama</label>
							<div class="col-md-6">
								<input class="form-control" type="text" name="name" readonly value="Adita Ahmad">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">NIM</label>
							<div class="col-md-6">
								<input class="form-control" type="text" name="name" readonly value="164060025">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Kelas</label>
							<div class="col-md-6">
								<input class="form-control" type="text" name="name" readonly value="Karyawan (Weekend)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Jurusan</label>
							<div class="col-md-6">
								<input class="form-control" type="text" name="name" readonly value="Teknik Informatika">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Semester</label>
							<div class="col-md-6">
								<input class="form-control" type="text" name="name" readonly value="VI (Enam)">
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="panel-body">
				<div class="alert alert-warning col-md-12">
				<p>* Catatan : Untuk menjalankan aplikasi ini dengan baik, diperlukan koneksi internet.</p>
			</div>
			</div>
		</div>
	</div>
</div>
@endsection
