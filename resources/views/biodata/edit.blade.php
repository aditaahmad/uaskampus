@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"><h4>Edit Biodata</h4></div>

				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form class="form-horizontal" role="form" method="post" action="{{ url('biodata/update/'.$value->id) }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label class="col-md-3 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" name="nama" class="form-control" value="{{ $value->nama }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Alamat</label>
							<div class="col-md-6">
								<input type="text" name="alamat" class="form-control" value="{{ $value->alamat }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Usia</label>
							<div class="col-md-6">
								<input type="text" name="usia" class="form-control" value="{{ $value->usia }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Hobby</label>
							<div class="col-md-6">
								<input type="text" name="hobby" class="form-control" value="{{ $value->hobby }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<button class="btn btn-primary" type="submit">Simpan</button>
								<a class="btn btn-link" href="{{ url('biodata') }}">Kembali</a>
							</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
