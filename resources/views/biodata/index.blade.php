@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"><h4>List Biodata</h4></div>

				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<a href="{{ url('biodata/add') }}" style="float: right;"><button class="btn btn-primary">Tambah Data Baru</button></a>
						</div>
					</div>
					<br>
					<div class="table-responsive">
						
						<table class="table table-hover table-condensed">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Usia</th>
									<th>Hobby</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							@if ($data->count() > 0)
								@foreach ($data as $value)
									<tr>
										<td>{{ $value['id'] }}</td>
										<td>{{ $value['nama'] }}</td>
										<td>{{ $value['alamat'] }}</td>
										<td>{{ $value['usia'] }}</td>
										<td>{{ $value['hobby'] }}</td>
										<td><a href="{{ url('biodata/edit/'.$value['id']) }}"><button class="btn btn-primary">Edit</button></a> <a href="{{ url('biodata/delete/'.$value['id']) }}"><button class="btn btn-danger">Hapus</button></a></td>
									</tr>
								@endforeach
							@else
								<tr>
									<td class="text-center" colspan="6">Tidak ada data biodata yang tersedia.</td>
								</tr>
							@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
