<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['prefix' => 'biodata', 'as' => 'biodata.'], function ($route) {
    
    $route->get('/', 'BiodataController@index');
    $route->get('add', 'BiodataController@create');
    $route->post('save', 'BiodataController@store');
    $route->get('edit/{id}', 'BiodataController@edit');
    $route->post('update/{id}', 'BiodataController@update');
    $route->get('delete/{id}', 'BiodataController@destroy');
});
