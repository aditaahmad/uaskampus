<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Biodata;
use Redirect;

class BiodataController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Biodata::all();

		return view('biodata.index', [
			'data' => $data
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('biodata.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = [
			'nama' => $request->nama,
			'alamat' => $request->alamat,
			'usia' => $request->usia,
			'hobby' => $request->hobby
		];

		Biodata::create($data);

		return redirect('biodata');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Biodata::find($id);

		return view('biodata.edit', [
			'value' => $data
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$data = Biodata::find($id);
		$data->update([
			'nama' => $request->nama,
			'alamat' => $request->alamat,
			'usia' => $request->usia,
			'hobby' => $request->hobby
		]);

		return redirect('biodata');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Biodata::find($id);
		$data->delete();

		return redirect('biodata');
	}

}
